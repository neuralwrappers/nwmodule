import torch as tr
import numpy as np
from typing import List, Dict

def isBaseOf(whatType, baseType):
	if not isinstance(whatType, type):
		whatType = type(whatType)
	if not isinstance(baseType, type):
		baseType = type(baseType)
	return baseType in type(object).mro(whatType)

def getOptimizerStr(optimizer):
	if isinstance(optimizer, dict):
		return ["Dict"]

	if optimizer is None:
		return ["None"]

	if isinstance(optimizer, tr.optim.SGD):
		groups = optimizer.param_groups[0]
		params = "Learning rate: %s, Momentum: %s, Dampening: %s, Weight Decay: %s, Nesterov: %s" % \
			(groups["lr"], groups["momentum"], groups["dampening"], groups["weight_decay"], groups["nesterov"])
		optimizerType = "SGD"
	elif isinstance(optimizer, (tr.optim.Adam, tr.optim.AdamW)):
		groups = optimizer.param_groups[0]
		params = "Learning rate: %s, Betas: %s, Eps: %s, Weight Decay: %s" % (groups["lr"], groups["betas"], \
			groups["eps"], groups["weight_decay"])
		optimizerType = {
			tr.optim.Adam : "Adam",
			tr.optim.AdamW : "AdamW"
		}[type(optimizer)]
	elif isinstance(optimizer, tr.optim.RMSprop):
		groups = optimizer.param_groups[0]
		params = "Learning rate: %s, Momentum: %s. Alpha: %s, Eps: %s, Weight Decay: %s" % (groups["lr"], \
			groups["momentum"], groups["alpha"], groups["eps"], groups["weight_decay"])
		optimizerType = "RMSprop"
	elif isinstance(optimizer, tr.optim.Optimizer):
		return str(optimizer)
	else:
		optimizerType = "Generic Optimizer"
		params = str(optimizer)

	return ["%s. %s" % (optimizerType, params)]


def getTrainableParameters(model):
	if not model.training:
		return {}

	trainableParameters = {}
	namedParams = dict(model.named_parameters())
	# Some PyTorch "weird" stuff. Basically this is a hack specifically for BatchNorm (Dropout not supported yet...).
	# BatchNorm parameters are not stored in named_parameters(), just in state_dict(), however in state_dict() we can't
	#  know if it's trainable or not. So, in order to keep all trainable parameters, we need to check if it's either
	#  a BN (we'll also store non-trainable BN, but that's okay) or if it's trainable (in named_params).
	def isBatchNormModuleTrainable(name):
		nonParametersNames = ["running_mean", "running_var", "num_batches_tracked"]
		if name.split(".")[-1] in nonParametersNames:
			# edges.10.model.module.0.conv7.1.running_mean => edges.10.model.module.0.conv7.1.weight is trainable?
			resName = ".".join(name.split(".")[0 : -1])
			potentialName = "%s.weight" % (resName)
			if potentialName in namedParams and namedParams[potentialName].requires_grad:
				return True
		return False

	for name in model.state_dict():
		if isBatchNormModuleTrainable(name):
			trainableParameters[name] = model.state_dict()[name]

		if (name in namedParams) and (namedParams[name].requires_grad):
			trainableParameters[name] = model.state_dict()[name]
	return trainableParameters

def computeNumParams(namedParams):
	numParams = 0
	for name in namedParams:
		param = namedParams[name]
		numParams += np.prod(param.shape)
	return numParams

def getNumParams(model):
	return computeNumParams(model.state_dict()), computeNumParams(getTrainableParameters(model))

def trModuleWrapper(module):
	from .models import FeedForwardNetwork
	class Model(FeedForwardNetwork):
		def __init__(self, _module):
			super().__init__()
			self._module = _module

		def forward(self, *args, **kwargs):
			return self._module(*args, **kwargs)

	res = Model(module)
	return res

def defaultBatchFn(x: List[Dict]) -> Dict:
    def _merge(x: List):
        if isinstance(x[0], (int, float, complex, np.ndarray, np.number, list)):
            return mergeFinal(x)
        elif isinstance(x[0], dict):
            return mergeDict(x)
        elif isinstance(x[0], type(None)):
            return None
        else:
            assert False, f"Unknown type {type(x[0])}"

    def mergeFinal(x: List):
        return np.stack(x, axis=0)

    def mergeDict(x: List[Dict]):
        assert isinstance(x[0], dict)
        Keys = list(x[0].keys())
        N = len(x)
        res = {k: _merge([x[i][k] for i in range(N)]) for k in Keys}
        return res

    return _merge(x)
