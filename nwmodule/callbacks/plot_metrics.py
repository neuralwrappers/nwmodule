from copy import deepcopy
from typing import List, Any, Dict
from overrides import overrides
import numpy as np
import torch as tr
import plotly.express as px
import plotly.graph_objects as go
from nwutils.primitives.dict import deep_dict_get
from pathlib import Path

from .callback import Callback
from ..types import EpochResultsType

class PlotMetrics(Callback):
    def __init__(self, metric_names: List[str], **kwargs):
        assert len(metric_names) > 0, "Expected a list of at least one metric which will be plotted."
        self.metric_names = []
        for metric_name in metric_names:
            if isinstance(metric_name, Callback):
                metric_name = metric_name
            self.metric_names.append(metric_name)
        super().__init__(name=str(self), **kwargs)

    @staticmethod
    def _add_bullet(scores: Dict, direction: str):
        usedValues = scores["Validation"] if "Validation" in scores else scores["Train"]
        assert direction in ("min", "max")
        if direction == "min":
            x, y = np.argmin(usedValues) + 1, np.min(usedValues)
        elif direction == "max":
            x, y = np.argmax(usedValues) + 1, np.max(usedValues)
        text = f"{float(y):.3f} @ epoch {x}"
        return px.scatter({"x": [float(x)], "y": [float(y)], "text": [text]}, x="x", y="y", text="text")

    @staticmethod
    def _get_scores(history, key, metric_name):
        _scores = [deep_dict_get(epoch_history[key], metric_name) for epoch_history in history]
        _scores = [x.mean() for x in _scores] if isinstance(_scores[0], np.ndarray) else _scores
        _scores = np.array(_scores)
        if len(np.isnan(_scores)) > 0:
            non_nan = _scores[~np.isnan(_scores)]
            median = 0 if len(non_nan) == 0 else np.median(non_nan)
            _scores[np.isnan(_scores)] = median
        return _scores

    @staticmethod
    def _plot_one_metric(model, metric_name: str, trainHistory: Dict, workingDirectory: Path):
        metric = model.getMetric(metric_name)
        num_epochs = len(trainHistory)
        hasValidation = "Validation" in trainHistory[0]

        scores = {
            "Train": PlotMetrics._get_scores(trainHistory, "Train", metric_name)
        }

        if hasValidation:
            scores["Validation"] = PlotMetrics._get_scores(trainHistory, "Validation", metric_name)

        X = np.arange(1, num_epochs + 1)
        try:
            fig_scores = px.line(scores, x=X, y=list(scores.keys()), labels={"x": "Epoch", "value": "Score", "variable": ""})
        except:
            breakpoint()
        fig_bullets = PlotMetrics._add_bullet(scores, metric.direction)

        fig = go.Figure(data=fig_scores.data + fig_bullets.data, layout_title_text=metric_name)
        file_name = f"{workingDirectory}/{metric_name}.png"
        fig.write_image(file_name)


    @overrides
    def on_epoch_end(self, model, epoch_results: EpochResultsType, working_directory: Path) -> Any:
        if not "Train" in epoch_results or len(model.getTrainHistory()) == 0:
            return

        history = deepcopy(model.getTrainHistory())
        history.append(epoch_results)
        for i in range(len(self.metric_names)):
            PlotMetrics._plot_one_metric(model, self.metric_names[i], history, working_directory)

    @overrides
    def on_epoch_start(self, model, working_directory: Path):
        for metric_name in self.metric_names:
            assert metric_name in model.getMetrics(), \
                f"Metric '{metric_name}' not in model metrics: {list(model.getMetrics())}"

    @overrides
    def on_iteration_start(self, **kwargs):
        pass

    @overrides
    def on_iteration_end(self, y: tr.Tensor, gt: tr.Tensor, **kwargs) -> Any:
        pass

    @overrides
    def onCallbackSave(self, **kwargs):
        self.directions = None

    @overrides
    def onCallbackLoad(self, additional, **kwargs):
        pass

    @overrides
    def __str__(self):
        assert len(self.metric_names) >= 1
        Str = str(self.metric_names[0])
        for i in range(len(self.metric_names)):
            Str += f", {self.metric_names[i]}"
        return f"PlotMetrics ({Str})"
