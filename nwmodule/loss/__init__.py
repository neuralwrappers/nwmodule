from .l1 import l1
from .l2 import l2
from .bce import bce, sigmoid_bce
from .nll import nll, softmax_nll