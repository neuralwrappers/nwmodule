import os
import pytest
import numpy as np
import torch as tr
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from pathlib import Path
from nwmodule.models import FeedForwardNetwork
from nwmodule.loss import softmax_nll
from nwmodule.utils import defaultBatchFn
from torchvision.datasets import MNIST as _MNIST
from torch.utils.data import DataLoader, Subset

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

try:
	MNIST_DATASET_PATH = os.environ["MNIST_DATASET_PATH"]
except Exception:
	MNIST_DATASET_PATH = "/tmp/mnist"

class MNIST(_MNIST):
    def __getitem__(self, index: int):
        item = super().__getitem__(index)
        image, label = item
        label = np.eye(10)[label]
        image = np.array(image, dtype=np.float32) / 255
        return {
            "data": {"images": image},
            "labels": label
        }

class ModelFC(FeedForwardNetwork):
    # (28, 28, 1) => (10, 1)
    def __init__(self, inputShape, outputNumClasses):
        super().__init__()

        self.inputShapeProd = int(np.prod(np.array(inputShape)))
        self.fc1 = nn.Linear(self.inputShapeProd, 100)
        self.fc2 = nn.Linear(100, 100)
        self.fc3 = nn.Linear(100, outputNumClasses)

    def forward(self, x):
        x = x["images"].view(-1, self.inputShapeProd)
        y1 = F.relu(self.fc1(x))
        y2 = F.relu(self.fc2(y1))
        y3 = self.fc3(y2)
        return y3

    def criterion(self, y, gt) -> tr.Tensor:
        return softmax_nll(y, gt).mean()

class TestMNISTClassifier:
    def test(self):
        reader = DataLoader(Subset(MNIST(Path(MNIST_DATASET_PATH).absolute(), download=True), indices=range(100)), \
            batch_size=10, collate_fn=defaultBatchFn, num_workers=4)
        model = ModelFC(inputShape=(28, 28, 1), outputNumClasses=10).to(device)
        model.setOptimizer(optim.SGD, lr=0.01)
        model.train_reader(reader, num_epochs=1)

def main():
    TestMNISTClassifier().test()

if __name__ == "__main__":
    main()