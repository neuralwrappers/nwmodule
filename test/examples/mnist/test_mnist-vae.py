import os
import h5py
import pytest
import numpy as np
from overrides import overrides
from pathlib import Path

import torch as tr
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torchvision.datasets import MNIST
from nwmodule.models import VariationalAutoencoderNetwork, GeneratorNetwork
from nwmodule.utils import defaultBatchFn
from torchvision.datasets import MNIST as _MNIST
from torch.utils.data import DataLoader, Subset

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

try:
	MNIST_DATASET_PATH = os.environ["MNIST_DATASET_PATH"]
except Exception:
	MNIST_DATASET_PATH = "/tmp/mnist"

class MNIST(_MNIST):
	def __getitem__(self, index: int):
		item = super().__getitem__(index)
		image, _ = item
		image = (np.array(image) > 0).astype(np.float32)
		return {
			"data": image,
			"labels": image
		}

class Encoder(nn.Module):
	def __init__(self, noise_size):
		super().__init__()
		self.noise_size = noise_size
		self.fc1 = nn.Linear(28 * 28, 100)
		self.fc2 = nn.Linear(100, 100)
		self.mean_fc = nn.Linear(100, noise_size)
		self.mean_std = nn.Linear(100, noise_size)

	def forward(self, x):
		x = x.view(-1, 28 * 28)
		y1 = F.relu(self.fc1(x))
		y2 = F.relu(self.fc2(y1))
		y_mean = self.mean_fc(y2)
		y_std = self.mean_std(y2)
		return y_mean, y_std

class Decoder(GeneratorNetwork):
	def __init__(self, noise_size):
		super().__init__(noise_size)
		self.fc1 = nn.Linear(noise_size, 300)
		self.fc2 = nn.Linear(300, 28 * 28)

	def forward(self, z_samples):
		y1 = F.relu(self.fc1(z_samples))
		y2 = self.fc2(y1)
		y_decoder = tr.sigmoid(y2)
		return y_decoder

class TestMNISTVAE:
	def test(self):
		reader = DataLoader(Subset(MNIST(Path(MNIST_DATASET_PATH), download=True), indices=range(100)), \
				batch_size=10, collate_fn=defaultBatchFn, num_workers=4)

		encoder = Encoder(noise_size=200)
		decoder = Decoder(noise_size=200)
		model = VariationalAutoencoderNetwork(encoder, decoder, \
			lossWeights={"latent" : 1 / (1000), "decoder" : 1}).to(device)
		model.setOptimizer(optim.SGD, lr=0.01)
		model.train_reader(reader, num_epochs=1)

def main():
	TestMNISTVAE().test()

if __name__ == "__main__":
	main()