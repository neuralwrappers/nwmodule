import os
import pytest
import numpy as np
import torch as tr
import torch.nn.functional as F
import torch.nn as nn
import torch.optim as optim
from pathlib import Path
from nwmodule.models import GenerativeAdversarialNetwork, FeedForwardNetwork
from nwmodule.models.generator_network import GeneratorNetwork
from nwmodule.utils import defaultBatchFn
from torchvision.datasets import MNIST as _MNIST
from torch.utils.data import DataLoader, Subset

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

try:
	MNIST_DATASET_PATH = os.environ["MNIST_DATASET_PATH"]
except Exception:
	MNIST_DATASET_PATH = "/tmp/mnist"

class MNIST(_MNIST):
	def __init__(self, trainPath, latentSpaceSize:int, *args, **kwargs):
		super().__init__(root=trainPath, *args, **kwargs)
		self.latentSpaceSize = latentSpaceSize

	def __getitem__(self, index: int):
		item = super().__getitem__(index)
		image, label = item
		label = np.eye(10)[label]
		image = (np.array(image, dtype=np.float32) / 255 - 0.5) * 2
		noise = np.random.randn(self.latentSpaceSize).astype(np.float32)
		return {
			"data": noise,
			"labels": image
		}

class GeneratorLinear(GeneratorNetwork):
	def __init__(self, inputSize, outputSize):
		super().__init__(inputSize)
		assert len(outputSize) == 3
		self.inputSize = inputSize
		self.outputSize = outputSize

		self.fc1 = nn.Linear(self.inputSize, 128)
		self.fc2 = nn.Linear(128, 256)
		self.bn2 = nn.BatchNorm1d(256)
		self.fc3 = nn.Linear(256, 512)
		self.bn3 = nn.BatchNorm1d(512)
		self.fc4 = nn.Linear(512, 1024)
		self.bn4 = nn.BatchNorm1d(1024)
		self.fc5 = nn.Linear(1024, outputSize[0] * outputSize[1] * outputSize[2])

	def forward(self, x):
		y1 = F.leaky_relu(self.fc1(x))
		y2 = F.leaky_relu(self.bn2(self.fc2(y1)), negative_slope=0.2)
		y3 = F.leaky_relu(self.bn3(self.fc3(y2)), negative_slope=0.2)
		y4 = F.leaky_relu(self.bn4(self.fc4(y3)), negative_slope=0.2)
		y5 = tr.tanh(self.fc5(y4))
		y5 = y5.view(-1, *self.outputSize)
		return y5

class DiscriminatorLinear(FeedForwardNetwork):
	def __init__(self, inputSize):
		super().__init__()
		assert len(inputSize) == 3
		self.inputSize = inputSize
		self.fc1 = nn.Linear(inputSize[0] * inputSize[1] * inputSize[2], 512)
		self.fc2 = nn.Linear(512, 256)
		self.fc3 = nn.Linear(256, 1)

	def forward(self, x):
		x = x.view(-1, self.inputSize[0] * self.inputSize[1] * self.inputSize[2])
		y1 = F.leaky_relu(self.fc1(x), negative_slope=0.2)
		y2 = F.leaky_relu(self.fc2(y1), negative_slope=0.2)
		y3 = tr.sigmoid(self.fc3(y2))
		y3 = y3.view(y3.shape[0])
		return y3

class TestMNISTGAN:
	def test(self):
		reader = DataLoader(Subset(MNIST(Path(MNIST_DATASET_PATH), download=True, latentSpaceSize=200), \
			indices=range(100)), batch_size=10, collate_fn=defaultBatchFn, num_workers=4)

		generatorModel = GeneratorLinear(inputSize=200, outputSize=(28, 28, 1))
		discriminatorModel = DiscriminatorLinear(inputSize=(28, 28, 1))

		# Define model
		model = GenerativeAdversarialNetwork(generator=generatorModel, discriminator=discriminatorModel).to(device)
		model.setOptimizer(optim.SGD, lr=0.01)
		model.train_reader(reader, num_epochs=1)

def main():
	TestMNISTGAN().test()

if __name__ == "__main__":
	main()