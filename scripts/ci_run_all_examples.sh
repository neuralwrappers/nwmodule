#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
ROOT_DIR=$SCRIPT_DIR/..
TRAIN_DIR=test
MNIST_DIR=/tmp/mnist
cd $ROOT_DIR/examples/mnist-classifier/
rm -rf $TRAIN_DIR;
NWMODULE_LOGLEVEL=3 python3 main.py train --datasetPath $MNIST_DIR --dir $TRAIN_DIR --numEpochs 10
cd $ROOT_DIR/examples/mnist-vae
rm -rf $TRAIN_DIR;
NWMODULE_LOGLEVEL=3 python3 main.py train --datasetPath $MNIST_DIR --dir $TRAIN_DIR --numEpochs 10
cd $ROOT_DIR/examples/mnist-gan
rm -rf $TRAIN_DIR;
NWMODULE_LOGLEVEL=3 python3 main.py train --datasetPath $MNIST_DIR --dir $TRAIN_DIR --numEpochs 10
