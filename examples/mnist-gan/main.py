import torch as tr
import numpy as np
import torch.optim as optim
from argparse import ArgumentParser
from media_processing_lib.image import image_resize, to_image, image_write
from functools import partial
from nwmodule.models import GenerativeAdversarialNetwork
from nwmodule.callbacks import SaveModels, PlotMetrics, RandomPlotEachEpoch
from nwutils.path import change_directory

from models import GeneratorLinear as Generator, DiscriminatorLinear as Discriminator
from reader import getReader
from test_model import test_model

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("type")
    parser.add_argument("--datasetPath", required=True)
    parser.add_argument("--latent_space_size", type=int, default=200)

    parser.add_argument("--dir", default="test")
    parser.add_argument("--batch_size", type=int, default=100)
    parser.add_argument("--numEpochs", type=int, default=200)
    parser.add_argument("--weights_file")

    args = parser.parse_args()
    assert args.type in ("train", "retrain", "test_model")

    return args

def plotFn(x, y, t, model, workingDirectory):
    # Generate 20 random gaussian inputs
    np_random_out_generator = model.inference_step(25).detach().cpu().numpy()
    images = np.array([to_image(x) for x in np_random_out_generator])
    images = images.reshape(5, 5, *images.shape[1: ])
    image = np.concatenate(np.concatenate(images, axis=1), axis=1)
    image = image_resize(image, height=512, width=None)
    image_write(image, f"{workingDirectory}/results.png")

def main():
    args = getArgs()

    # Define reader, generator and callbacks
    reader = getReader(args.datasetPath, latent_space_size=args.latent_space_size,
                       batch_size=args.batch_size, train=True)
    validation_reader = getReader(args.datasetPath, latent_space_size=args.latent_space_size,
                                  batch_size=args.batch_size, train=False)
    print(reader.dataset)
    print(validation_reader.dataset)

    generatorModel = Generator(inputSize=args.latent_space_size, outputSize=(28, 28, 1))
    discriminatorModel = Discriminator(inputSize=(28, 28, 1))

    # Define model
    model = GenerativeAdversarialNetwork(generator=generatorModel, discriminator=discriminatorModel).to(device)
    model.setOptimizer(optim.SGD, lr=0.01)
    model.addCallbacks([
        SaveModels("last", "Generator Loss"),
        PlotMetrics(["Loss", "Generator Loss", "Discriminator Loss"]),
        RandomPlotEachEpoch(partial(plotFn, model=model))
    ])
    print(model.summary())

    if args.type == "train":
        change_directory(args.dir, expect_exist=False)
        model.train_reader(reader, num_epochs=args.numEpochs, validation_reader=validation_reader)
    elif args.type == "retrain":
        model.loadModel(args.weights_file)
        model.train_reader(reader, num_epochs=args.numEpochs, validation_reader=validation_reader)
    elif args.type == "test_model":
        model.loadModel(args.weights_file)
        test_model(args, model)

if __name__ == "__main__":
    main()