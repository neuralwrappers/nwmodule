from media_processing_lib.image import image_add_title, image_resize
import cv2
import numpy as np

def test_model(args, model):
    while True:
        randomInputsG = np.random.randn(args.batch_size, args.latent_space_size).astype(np.float32)
        randomOutG = model.generator.npForward(randomInputsG)
        outD = model.discriminator.npForward(randomOutG)
        for j in range(args.batch_size):
            image = randomOutG[j, :, :, 0]
            image = image_resize(image, height=300, width=300)
            image = image_add_title(image, f"Discriminator: {outD[j]}")
            cv2.imshow(image)
