import numpy as np
from functools import partial
from overrides import overrides
from pathlib import Path
from nwmodule.utils import defaultBatchFn
from torchvision.datasets import MNIST as _MNIST
from torch.utils.data import DataLoader

class MNIST(_MNIST):
    def __init__(self, train_path, latent_space_size:int, *args, **kwargs):
        super().__init__(root=train_path, *args, **kwargs)
        self.latent_space_size = latent_space_size

    def __getitem__(self, index: int):
        item = super().__getitem__(index)
        image, label = item
        label = np.eye(10)[label]
        # For some reasons, results are much better if provided data is in range -1 : 1 (not 0 : 1 or standardized).
        image = (np.array(image, dtype=np.float32) / 255 - 0.5) * 2
        return {
            "data": image,
            "labels": image
        }

def getReader(datasetPath:Path, latent_space_size: int, batch_size: int, train: bool) -> MNIST:
    reader = DataLoader(MNIST(datasetPath, latent_space_size=latent_space_size, train=train, download=True),
                        batch_size=batch_size, collate_fn=defaultBatchFn, num_workers=4)
    return reader
