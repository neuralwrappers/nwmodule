import torch as tr
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

from nwmodule.models import FeedForwardNetwork, VariationalAutoencoderNetwork, GeneratorNetwork
from nwmodule.callbacks import SaveModels, PlotMetrics, SaveHistory

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

class Encoder(FeedForwardNetwork):
	def __init__(self, noise_size):
		super().__init__()
		self.noise_size = noise_size
		self.fc1 = nn.Linear(28 * 28, 100)
		self.fc2 = nn.Linear(100, 100)
		self.mean_fc = nn.Linear(100, noise_size)
		self.mean_std = nn.Linear(100, noise_size)

	def forward(self, x):
		x = x.view(-1, 28 * 28)
		y1 = F.relu(self.fc1(x))
		y2 = F.relu(self.fc2(y1))
		y_mean = self.mean_fc(y2)
		y_std = self.mean_std(y2)
		return y_mean, y_std

class Decoder(GeneratorNetwork):
	def __init__(self, noise_size):
		super().__init__(noise_size)
		self.fc1 = nn.Linear(noise_size, 300)
		self.fc2 = nn.Linear(300, 28 * 28)

	def forward(self, z_samples):
		y1 = F.relu(self.fc1(z_samples))
		y2 = self.fc2(y1)
		y_decoder = tr.sigmoid(y2)
		return y_decoder

def getModel(args):
	encoder = Encoder(noise_size=args.noise_size)
	decoder = Decoder(noise_size=args.noise_size)
	model = VariationalAutoencoderNetwork(encoder, decoder, \
		lossWeights={"latent" : 1 / 1000, "decoder" : 1}).to(device)
	model.setOptimizer(optim.AdamW, lr=0.00001)
	model.addCallbacks([SaveModels("last", "Loss"), SaveHistory("history.txt"), \
		PlotMetrics(["Loss", "Reconstruction Loss", "Latent Loss"])])
	return model
