# Implementation of Variational Auto Encoder (VAE) based on http://kvfrans.com/variational-autoencoders-explained/
#  and https://arxiv.org/abs/1606.05908 using binary MNIST.
# Notes: loss is used using the empirical (latent_loss / reconstruction_loss) * reconstruction_loss + latent_loss
# If you get NaNs during training, lower the learning rate or change the optimizer.

import numpy as np
from argparse import ArgumentParser
from functools import partial
from nwutils.path import change_directory
from media_processing_lib.image import to_image, image_resize, image_write
from nwmodule.callbacks import RandomPlotEachEpoch

from reader import getReader
from models import getModel

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("type")
    parser.add_argument("--datasetPath", required=True)
    parser.add_argument("--noise_size", type=int, default=100)
    parser.add_argument("--batchSize", type=int, default=20)
    parser.add_argument("--numEpochs", type=int, default=100)
    parser.add_argument("--dir", default="test")
    parser.add_argument("--weightsFile")
    args = parser.parse_args()
    return args

def plotFn(x, y, t, model, workingDirectory):
    # Generate 20 random gaussian inputs
    np_random_out_generator = model.inference_step(25).detach().cpu().numpy()
    np_random_out_generator = np_random_out_generator.reshape(-1, 28, 28) > 0.5
    images = np.array([to_image(x) for x in np_random_out_generator])
    images = images.reshape(5, 5, *images.shape[1: ])
    image = np.concatenate(np.concatenate(images, axis=1), axis=1)
    image = image_resize(image, height=512, width=None)
    image_write(image, f"{workingDirectory}/results.png")

def main():
    args = getArgs()

    reader = getReader(args.datasetPath, args.batchSize, train=True, download=True)
    validation_reader = getReader(args.datasetPath, args.batchSize, train=False, download=True)
    print(reader.dataset)
    print(validation_reader.dataset)

    model = getModel(args)
    model.addCallback(RandomPlotEachEpoch(partial(plotFn, model=model)))
    print(model.summary())

    if args.type == "train":
        change_directory(args.dir, expect_exist=False)
        model.train_reader(reader, num_epochs=args.numEpochs, validation_reader=validation_reader)
    elif args.type == "retrain":
        model.loadModel(args.weightsFile)
        change_directory(args.dir, expect_exist=True)
        model.train_reader(reader, num_epochs=args.numEpochs, validation_reader=validation_reader)
    elif args.type == "test":
        model.loadModel(args.weightsFile)
        res = model.test_reader(reader)
        print(res)

if __name__ == "__main__":
    main()
