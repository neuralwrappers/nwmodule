import h5py
import numpy as np
from pathlib import Path
from torch.utils.data import DataLoader
from nwmodule.utils import defaultBatchFn
from torchvision.datasets import MNIST as _MNIST

class MNIST(_MNIST):
	def __getitem__(self, index: int):
		item = super().__getitem__(index)
		image, _ = item
		image = (np.array(image) > 0).astype(np.float32)
		return {
			"data": image,
			"labels": image
		}

def getReader(dataset_path: Path, batch_size: int, **kwargs):
	reader = DataLoader(MNIST(dataset_path, **kwargs), batch_size=batch_size, collate_fn=defaultBatchFn, num_workers=4)
	return reader
