import numpy as np
import re
from overrides import overrides
from typing import List
from pathlib import Path
from torch.utils.data import DataLoader, Dataset
from nwmodule.utils import defaultBatchFn

class Reader(Dataset):
	def __init__(self, path:str, context:int):
		self.datasetPath = path
		self.context = context
		self.dictionary = self.getCuratedDictionary()
		self.ixToWord = dict(zip(range(len(self.dictionary)), self.dictionary))
		self.wordToIx = dict(zip(self.dictionary, range(len(self.dictionary))))

	def getCuratedDictionary(self):
		f = open(self.datasetPath, "r").read()
		f = re.sub('[^0-9a-zA-Z]+', ' ', f).strip().lower()
		f = f.split(" ")
		f = list(set(f))
		return f

	def __len__(self):
		return len(self.dictionary) - self.context - 1

	def __getitem__(self, index):
		sentence = self.dictionary[index : index + self.context]
		sentenceIx = [self.wordToIx[w] for w in sentence]

		context = sentenceIx[0 : -1]
		word = sentenceIx[-1]
		return {
			"data": {"context": context, "word": word},
			"labels": None
		}

def getReader(datasetPath:Path, context:int, batchSize:int) -> DataLoader:
	reader = Reader(datasetPath, context)
	reader = DataLoader(reader, collate_fn=defaultBatchFn, batch_size=batchSize)
	return reader
