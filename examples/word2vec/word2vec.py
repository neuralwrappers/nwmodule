import torch as tr
import torch.nn as nn
import torch.nn.functional as F
from nwmodule.models import FeedForwardNetwork
from nwmodule.loss import softmax_nll

class Word2Vec(FeedForwardNetwork):
	def __init__(self, dictionarySize:int, embeddingSize:int, negativeSampleCount:int):
		super().__init__()
		self.dictionarySize = dictionarySize
		self.embeddingSize = embeddingSize
		self.negativeSampleCount = negativeSampleCount
		self.embIn = nn.Linear(in_features=dictionarySize, out_features=embeddingSize, bias=False)
		self.embOut = nn.Linear(in_features=2 * embeddingSize, out_features=1, bias=False)

		# self.labels = tr.zeros(negativeSampleCount + 1).type(tr.bool).to(device)
		# self.labels[0] = 1

	# TODO: Weighted sampling
	def sampleNegativeWords(self, MB):
		negative = tr.randint(self.dictionarySize, size=(self.negativeSampleCount * MB, )).view(MB, -1)
		negative = negative.to(self.getDevice())
		return negative

	@staticmethod
	def simFn(a, b):
		return ((a-b)**2).mean()

	@staticmethod
	def lossFn(y, t):
		# Negative log-likeklihood (used for softmax+NLL for classification), expecting targets are one-hot encoded
		y = F.softmax(y, dim=1)
		t = t.type(tr.bool)
		return (-tr.log(y[t] + 1e-5)).mean()

	def networkAlgorithm(self, trInputs, trLabels, isTraining:bool, isOptimizing:bool):
		words = trInputs["word"]
		context = trInputs["context"]
		MB = len(words)
		# negativeWords = self.sampleNegativeWords(self.dictionarySize, self.negativeSampleCount * MB).view(MB, -1).detach()
		negativeWords = self.sampleNegativeWords(MB)

		# MB x 1 x emb
		wordEmbedding = self.forward(words)
		# MB x context x emb
		contextEmbedding = self.forward(context)
		# MB x negativeSampleCount x emb
		negativeWordsEmbedding = self.forward(negativeWords)
		# MB x 1 x emb
		meanContextEmbedding = contextEmbedding.mean(dim=1)

		posDiff = self.embOut(tr.cat([wordEmbedding, meanContextEmbedding], dim=1))
		negDiffIn = tr.cat([negativeWordsEmbedding,  meanContextEmbedding.unsqueeze(1)
			.repeat(1, self.negativeSampleCount, 1)], dim=2)
		negDiff = self.embOut(negDiffIn)[:, :, 0]
		both = tr.cat([negDiff, posDiff], dim=1)
		labels = tr.zeros(both.shape)
		labels[:, -1] = 1
		L = softmax_nll(both, labels).mean()

		self.updateOptimizer(L, isTraining, isOptimizing)
		return wordEmbedding, L

	# Returns the embedding given a batch of words as indexes
	def forward(self, x):
		# MB => MB x dictSize (one-hot)
		y = F.one_hot(x, num_classes=self.dictionarySize).type(tr.float)
		# MB x dictSize => mb x embeddingSize
		y = self.embIn(y)
		return y

