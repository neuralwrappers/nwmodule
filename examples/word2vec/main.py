import numpy as np
import torch as tr
import torch.optim as optim
from argparse import ArgumentParser

from reader import getReader
from word2vec import Word2Vec

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--datasetPath", required=True)
    parser.add_argument("--embeddingSize", type=int, default=100)
    parser.add_argument("--context", type=int, default=3)
    parser.add_argument("--negativeSampleCount", type=int, default=1000)
    parser.add_argument("--batchSize", type=int, default=30)
    parser.add_argument("--numEpochs", type=int, default=10)
    args = parser.parse_args()
    return args

def main():
    args = getArgs()
    reader = getReader(args.datasetPath, args.context, args.batchSize)
    model = Word2Vec(dictionarySize=len(reader.dataset.dictionary), embeddingSize=args.embeddingSize, \
        negativeSampleCount=args.negativeSampleCount).to(device)
    model.setOptimizer(optim.SGD, lr=0.0001)
    model.train_reader(reader, args.numEpochs)

if __name__ == "__main__":
    main()