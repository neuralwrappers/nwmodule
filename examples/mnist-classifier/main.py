import numpy as np
from argparse import ArgumentParser
from torch.utils.data import Subset, DataLoader, random_split
from torchvision.datasets import MNIST as _MNIST
from nwutils.path import change_directory
from nwmodule.utils import defaultBatchFn

from models import getModel

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("type", help="Options: train, retrain, test")
    parser.add_argument("--modelType", help="Options: model_fc, model_conv", default="model_fc")
    parser.add_argument("--datasetPath", required=True)
    parser.add_argument("--dir", help="Working directory", required=True)
    parser.add_argument("--weightsFile")
    parser.add_argument("--numEpochs", type=int, default=100)
    parser.add_argument("--batchSize", type=int, default=20)
    parser.add_argument("--debug", type=int, default=0)

    args = parser.parse_args()

    assert args.type in ("train", "test", "retrain")
    assert args.modelType in ("model_fc", "model_conv")
    if args.type in ("test", "retrain"):
        assert not args.weightsFile is None
    if args.type in ("train", "retrain"):
        assert not args.numEpochs is None
    return args

class MNIST(_MNIST):
    def __getitem__(self, index: int):
        item = super().__getitem__(index)
        image, label = item
        label = np.eye(10)[label]
        image = np.array(image, dtype=np.float32) / 255
        return {
            "data": {"images": image},
            "labels": label
        }

def getReader(args):
    train = True if args.type != "test" else False
    reader = MNIST(args.datasetPath, train=train, download=True)
    print(reader)
    reader = Subset(reader, indices=range(10)) if args.debug else reader
    if args.type in ("train", "retrain"):
        nTrain, nValidation = int(0.8 * len(reader)), len(reader) - int(0.8 * len(reader))
        train, validation = random_split(reader, lengths=[nTrain, nValidation])
        train = DataLoader(train, collate_fn=defaultBatchFn, batch_size=args.batchSize, num_workers=4)
        validation = DataLoader(validation, collate_fn=defaultBatchFn, batch_size=args.batchSize, num_workers=4)
        return train, validation
    else:
        return DataLoader(reader, collate_fn=defaultBatchFn, batch_size=args.batchSize, num_workers=4)

def main():
    args = getArgs()
    reader = getReader(args)
    model = getModel(args)

    if args.type == "train":
        train_reader, validation_reader = reader
        change_directory(args.dir, expect_exist=False)
        model.train_reader(train_reader, num_epochs=args.numEpochs, validation_reader=validation_reader)
    elif args.type == "retrain":
        train_reader, validation_reader = reader
        model.loadModel(args.weightsFile)
        change_directory(args.dir)
        model.train_reader(train_reader, num_epochs=args.numEpochs, validation_reader=validation_reader)
    elif args.type == "test":
        validation_reader = getReader(args)
        model.loadModel(args.weightsFile)
        metrics = model.testReader(validation_reader)
        print(f"Metrics: {metrics}")

if __name__ == "__main__":
    main()
