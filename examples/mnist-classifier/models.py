import numpy as np
import torch as tr
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from media_processing_lib.image import to_image, image_write, image_add_title, image_resize

from nwmodule.models import FeedForwardNetwork
from nwmodule.callbacks import SaveModels, SaveHistory, PlotMetrics, RandomPlotEachEpoch
from nwmodule.schedulers import ReduceLROnPlateau as scheduler
from nwmodule.loss import softmax_nll
from torchmetrics.functional import accuracy

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def plotFn(x, y, t, workingDirectory):
	cnt = 0
	MB = len(y)
	x = x["images"].cpu().numpy()
	y = F.softmax(y, dim=-1).detach().cpu().numpy()
	for i in range(MB):
		cnt += 1
		ix = np.argmax(t[i], axis=-1)
		thisPred = y[i][ix]
		thisImg = image_resize(to_image(x[i]), height=300, width=300)
		thisImg = image_add_title(thisImg, f"Label: {ix}. Result: {thisPred:.3f}", size_px=30)
		image_write(thisImg, f"{workingDirectory}/{cnt}.png")

def getModel(args):
	model = {
		"model_fc" : ModelFC(inputShape=(28, 28, 1), outputNumClasses=10),
		"model_conv" : ModelConv(inputShape=(28, 28, 1), outputNumClasses=10)
	}[args.modelType].to(device)
	model.criterion = lambda y, t: softmax_nll(y, t).mean()
	model.addMetric("Accuracy", lambda y, t: accuracy(y.argmax(-1), t.argmax(-1)), "max")
	model.setOptimizer(optim.SGD, momentum=0.5, lr=0.1)
	model.setOptimizerScheduler(scheduler(model, metric_name="Loss", factor=2, patience=5, verbose=True))
	callbacks = [SaveHistory("history.txt"), PlotMetrics(["Loss", "Accuracy"]), \
		SaveModels("best", "Loss"), SaveModels("last", "Loss"), RandomPlotEachEpoch(plotFn)]
	model.addCallbacks(callbacks)
	print(model.summary())
	return model

class ModelFC(FeedForwardNetwork):
	# (28, 28, 1) => (10, 1)
	def __init__(self, inputShape, outputNumClasses):
		super().__init__()

		self.inputShapeProd = int(np.prod(inputShape))
		self.fc1 = nn.Linear(self.inputShapeProd, 100)
		self.fc2 = nn.Linear(100, 100)
		self.fc3 = nn.Linear(100, outputNumClasses)

	def forward(self, x):
		x = x["images"].view(-1, self.inputShapeProd)
		y1 = F.relu(self.fc1(x))
		y2 = F.relu(self.fc2(y1))
		y3 = self.fc3(y2)
		return y3

class ModelConv(FeedForwardNetwork):
	def __init__(self, inputShape, outputNumClasses):
		super().__init__()

		if len(inputShape) == 2:
			inputShape = (*inputShape, 1)

		self.inputShape = inputShape
		self.fc1InputShape = (inputShape[0] - 4) * (inputShape[1] - 4) * 10

		self.conv1 = nn.Conv2d(in_channels=inputShape[2], out_channels=50, kernel_size=3, stride=1)
		self.conv2 = nn.Conv2d(in_channels=50, out_channels=10, kernel_size=3, stride=1)
		self.fc1 = nn.Linear(self.fc1InputShape, 100)
		self.fc2 = nn.Linear(100, outputNumClasses)

	def forward(self, x):
		x = x["images"].view(-1, self.inputShape[2], self.inputShape[0], self.inputShape[1])
		y1 = F.relu(self.conv1(x))
		y2 = F.relu(self.conv2(y1))
		y2 = y2.view(-1, self.fc1InputShape)
		y3 = F.relu(self.fc1(y2))
		y4 = self.fc2(y3)
		return y4
