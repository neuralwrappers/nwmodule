import numpy as np
from pathlib import Path
from numpy.core.numeric import indices
from torch.utils.data import Dataset, DataLoader, Subset
from nwmodule.utils import defaultBatchFn
from nwutils.data import toCategorical

from utils import sentenceToVector, sampleSentence

class Reader(Dataset):
	def __init__(self, path:Path, sequenceSize:int, stepsPerEpoch:int):
		self.datasetPath = path
		self.sequenceSize = sequenceSize
		self.stepsPerEpoch = stepsPerEpoch
		self.f = open(path, "r").read()
		chars = sorted(list(set(self.f)))
		self.charToIx = dict(zip(chars, range(len(chars))))
		self.ixToChar = dict(zip(range(len(chars)), chars))

	def __len__(self):
		return self.stepsPerEpoch

	def __getitem__(self, index):
		sentence = sampleSentence(self.f, self.sequenceSize)
		X, t = sentence[0 : -1], sentence[1 : ]
		X = sentenceToVector(X, self.charToIx).astype(np.float32)
		t = sentenceToVector(t, self.charToIx).astype(np.bool)
		return {"data":X, "labels":t}

def getReader(datasetPath:Path, sequenceSize:int, stepsPerEpoch:int, batchSize:int, debug:bool=False):
	stepsPerEpoch = 10 if debug else stepsPerEpoch
	reader = Reader(datasetPath, sequenceSize, stepsPerEpoch)
	reader = DataLoader(reader, batch_size=batchSize, collate_fn=defaultBatchFn)
	return reader