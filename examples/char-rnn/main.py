# Char-rnn like the one from: http://karpathy.github.io/2015/05/21/rnn-effectiveness/
# Download input txt frile from that blog (i.e. Shakespeare)
import numpy as np
import torch as tr
import torch.optim as optim
import time
from argparse import ArgumentParser

from nwmodule.models import FeedForwardNetwork
from nwmodule.callbacks import SaveModels, PlotMetrics

from model import Model
from reader import getReader
from utils import SampleCallback, sample

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def lossFn(output, target):
	# Batched binary cross entropy
	output, hidden = output
	return -tr.log(output[target] + 1e-5).mean()

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("datasetPath")
	parser.add_argument("--weightsFile")
	parser.add_argument("--batchSize", type=int, default=5)
	parser.add_argument("--numEpochs", type=int, default=100)
	parser.add_argument("--stepsPerEpoch", type=int, default=10000)
	parser.add_argument("--sequenceSize", type=int, default=20)
	parser.add_argument("--seedText")
	parser.add_argument("--cellType", default="LSTM")
	parser.add_argument("--debug", type=int, default=0)
	args = parser.parse_args()
	return args

def main():
	args = getArgs()
	reader = getReader(args.datasetPath, args.sequenceSize, args.stepsPerEpoch, args.batchSize, args.debug)

	model = Model(cellType=args.cellType, inputSize=len(reader.dataset.charToIx), hiddenSize=100).to(device)
	model.setOptimizer(optim.Adam, lr=0.01)
	model.setCriterion(lossFn)
	model.addCallbacks([SaveModels(mode="last", metricName="Loss"), \
		SampleCallback(reader.dataset.f, args.sequenceSize, numIterations=200), PlotMetrics(["Loss"])])
	print(model.summary())

	if args.type == "train":
		model.train_reader(reader, num_epochs=args.numEpochs)
	elif args.type == "test":
		model.loadModel(args.weightsFile)

		while True:
			result = sample(model, reader, numIters=200, seedText=args.seedText)
			print(result + "\n___________________________________________________________________")
			time.sleep(1)

if __name__ == "__main__":
	main()