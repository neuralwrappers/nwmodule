import numpy as np
import torch as tr
from nwmodule.callbacks import Callback
from nwmodule.nwmodule import NWModule
from nwutils.data import toCategorical
from typing import Dict, List, Tuple

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def sentenceToVector(sentence:str, charToIx:Dict[str, int]) -> np.ndarray:
	res = []
	for c in sentence:
		v = charToIx[c]
		v = toCategorical([v], numClasses=len(charToIx))[0]
		res.append(v)
	res = np.array(res, dtype=np.float32)
	return res

def vectorToSentence(vector:np.ndarray, ixToChar:Dict[int, str]) -> str:
	assert len(vector.shape) == 2, f"TxOneHot. Shape: {vector.shape}"
	res = ""
	for v in vector:
		c = np.argmax(v)
		c = ixToChar[c]
		res += c
	return res

def sampleSentence(pool:str, sequenceSize:int) -> str:
	startIx = np.random.randint(0, len(pool) - sequenceSize - 1)
	sentence = pool[startIx : startIx + sequenceSize]
	return sentence

def sample(model:NWModule, seedText:str, charToIx:Dict[str, int], ixToChar:Dict[int, str], numIters:int) \
	-> Tuple[str, str]:
	npSeedText = np.expand_dims(sentenceToVector(seedText, charToIx), axis=0)
	hprev = model.npForward(npSeedText)[1]

	result = ""
	output = np.expand_dims(npSeedText[:, -1], axis=1)
	Max = 0
	for i in range(numIters):
		output, hprev = model.npForward(output, hprev)
		charIndex = np.random.choice(range(len(charToIx)), p=output.flatten())
		Max = (Max * i + output.max()) / (i + 1)
		result += ixToChar[charIndex]
	print(f"MAX: {Max}")
	return seedText, result

class SampleCallback(Callback):
	def __init__(self, pool:List[str], sequenceSize:int, numIterations:int):
		super().__init__()
		self.pool = pool
		chars = sorted(list(set(self.pool)))
		self.charToIx = dict(zip(chars, range(len(chars))))
		self.ixToChar = dict(zip(range(len(chars)), chars))

		self.sequenceSize = sequenceSize
		self.numIterations = numIterations

	def onEpochEnd(self, **kwargs):
		seedText = sampleSentence(self.pool, sequenceSize=self.sequenceSize)
		seed, result = sample(kwargs["model"], seedText, self.charToIx, self.ixToChar, self.numIterations)
		print(f"\nSeed: {seed}\nResult: {result}\n______________________________________________________________")

	def onEpochStart(self, model, workingDirectory, isTraining: bool):
		pass

	def onIterationStart(self, **kwargs):
		pass

	def onIterationEnd(self, results: np.ndarray, labels: np.ndarray, **kwargs) -> np.ndarray:
		pass
